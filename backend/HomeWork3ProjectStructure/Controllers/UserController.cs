﻿using AutoMapper;
using Common.DTO;
using HomeWork3ProjectStructure.Commands;
using HomeWork3ProjectStructure.DAL;
using HomeWork3ProjectStructure.Handlers;
using HomeWork3ProjectStructure.Processors;
using HomeWork3ProjectStructure.Queries;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;

// For more information on enabling Web API for empty users, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace HomeWork3UserStructure.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly QuerieProcessor _querieProcessor;
        private readonly CommandProcessor _commandProcessor;

        public UserController(ProjectsDbContext context, IMapper mapper)
        {
            _querieProcessor = new QuerieProcessor(new QuerieHandler(context, mapper));
            _commandProcessor = new CommandProcessor(new CommandHandler(context, mapper));

        }
        // GET: api/<UserController>
        [HttpGet]
        public async System.Threading.Tasks.Task<string> GetAsync()
        {
            return JsonConvert.SerializeObject(await _querieProcessor.ProcessedAsync(new GetUsersQuerie()));
        }

        // GET api/<UserController>/5
        [HttpGet("{id}")]
        public async System.Threading.Tasks.Task<string> GetAsync(int id)
        {
            return JsonConvert.SerializeObject(await _querieProcessor.ProcessedAsync(new GetTaskByIdQuerie() { Id = id }));
        }

        // POST api/<UserController>
        [HttpPost]
        public async System.Threading.Tasks.Task<string> PostAsync([FromBody] UserDTO user)
        {
            return JsonConvert.SerializeObject(await _commandProcessor.ProcessedAsync(new CreateUserCommand() { user = user }));
        }

        // PUT api/<UserController>/5
        [HttpPut]
        public async System.Threading.Tasks.Task<string> Put([FromBody] UserDTO user)
        {
            return JsonConvert.SerializeObject(await _commandProcessor.ProcessedAsync(new UpdateUserCommand() { user = user }));

        }

        // DELETE api/<UserController>/5
        [HttpDelete("{id}")]
        public async System.Threading.Tasks.Task<IActionResult> DeleteAsync(int id)
        {
            try
            {
                await _commandProcessor.ProcessedAsync(new DeleteUserByIdCommand() { Id = id });
                return Ok();
            }
            catch (ArgumentException)
            {
                return NoContent();
            }
        }

        [HttpDelete]
        public async System.Threading.Tasks.Task<bool> Delete([FromBody] UserDTO user)
        {
            return await _commandProcessor.ProcessedAsync(new DeleteUserByItemCommand() { user = user });

        }
    }
}
