export class Task {
    Id : number;
    ProjectId : number;
    PerformerId : number;
    Name : string;
    Description :string;
    State : number;
    CreatedAt : Date;
    FinishedAt : Date;
    
    constructor(Id : number,
        ProjectId : number,
        PerformerId : number,
        Name : string,
        Description :string,
        State : number,
        CreatedAt : Date,
        FinishedAt : Date){
            this.Id =  Id;
            this.ProjectId =  ProjectId;
            this.PerformerId =  PerformerId;
            this.Name =  Name;
            this.Description =  Description;
            this.State =  State;
            this.CreatedAt =  CreatedAt;
            this.FinishedAt =  FinishedAt;
    }
}
