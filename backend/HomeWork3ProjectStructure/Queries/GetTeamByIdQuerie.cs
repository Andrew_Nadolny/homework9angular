﻿using Common.DTO;
using HomeWork3ProjectStructure.Interfaces;
using System.Threading.Tasks;

namespace HomeWork3ProjectStructure.Queries
{
    public class GetTeamByIdQuerie : IQuerie<Task<TeamDTO>>
    {
        public int Id { get; set; }
    }
}
