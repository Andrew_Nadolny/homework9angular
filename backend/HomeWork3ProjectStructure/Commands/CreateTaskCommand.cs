﻿using Common.DTO;
using HomeWork3ProjectStructure.Interfaces;
using System.Threading.Tasks;

namespace HomeWork3ProjectStructure.Commands
{
    public class CreateTaskCommand : ICommand<Task<TaskDTO>>
    {
        public TaskDTO task { get; set; }
    }
}
