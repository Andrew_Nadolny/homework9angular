﻿using Common.DTO;
using HomeWork3ProjectStructure.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HomeWork3ProjectStructure.Queries
{
    public class GetUserTasksWithShortNameByUserIdQuerie : IQuerie<Task<List<TaskDTO>>>
    {
        public int Id { get; set; }
    }
}
