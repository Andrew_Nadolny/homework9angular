﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Common.Models
{
    public class Project : Entity
    {
        public int AuthorId { get; set; }
        public int TeamId { get; set; }
        public List<Task> Tasks { get; set; }
        [MaxLength(100), MinLength(1)]
        public string Name { get; set; }
        [MaxLength(100), MinLength(1)]
        public string Description { get; set; }
        public DateTime Deadline { get; set; }
        public DateTime CreatedAt { get; set; }

        public override string ToString()
        {
            return string.Format("\nProject: [Id:{0}, Author:{1}, Team:{2}, Tasks:{3}, Name:{4}, Description:{5}, Deadline:{6}, CreateAt:{7}]\n",
                Id,
                AuthorId,
                TeamId,
                Tasks?.ToString(),
                Name,
                Description,
                Deadline,
                CreatedAt);
        }
    }
}
