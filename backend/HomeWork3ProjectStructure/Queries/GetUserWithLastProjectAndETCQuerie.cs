﻿using Common.DTO;
using HomeWork3ProjectStructure.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HomeWork3ProjectStructure.Queries
{
    public class GetUserWithLastProjectAndETCQuerie : IQuerie<Task<List<(UserDTO User, ProjectDTO LastProject, List<TaskDTO> LastProjectTask, int CountOfUnfinishedAndCanceledTasks, TaskDTO LongesUserTask)>>>
    {

    }
}
