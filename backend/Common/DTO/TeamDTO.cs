﻿using System;

namespace Common.DTO
{
    public class TeamDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }

        public override bool Equals(object obj)
        {
            if ((obj == null) || !this.GetType().Equals(obj.GetType()))
            {
                return false;
            }
            else
            {
                TeamDTO secondTeamDTO = (TeamDTO)obj;
                if (Id == secondTeamDTO.Id
                    && CreatedAt == secondTeamDTO.CreatedAt
                    && Name == secondTeamDTO.Name)
                {
                    return true;
                }
            }
            return false;
        }
    }
}

