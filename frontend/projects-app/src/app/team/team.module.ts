import { NgModule, Pipe } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TeamComponent } from './team/team.component';
import { TeamListComponent } from './team-list/team-list.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { DatepipeModule } from '../datepipe/datepipe.module';



@NgModule({
  declarations: [
    TeamComponent,
    TeamListComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    DatepipeModule
  ],
  exports: [TeamListComponent]
})
export class TeamModule { }
