﻿using Common.DTO;
using HomeWork3ProjectStructure.Interfaces;
using System.Threading.Tasks;

namespace HomeWork3ProjectStructure.Commands
{
    public class CreateTeamCommand : ICommand<Task<TeamDTO>>
    {
        public TeamDTO team { get; set; }
    }
}
