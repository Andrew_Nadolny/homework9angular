﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace HomeWork3ProjectStructure.Migrations
{
    public partial class AddSeedData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 1, 1, new DateTime(2020, 8, 25, 20, 49, 50, 451, DateTimeKind.Local).AddTicks(8054), new DateTime(2021, 9, 12, 22, 17, 47, 233, DateTimeKind.Local).AddTicks(5223), "Et doloribus et temporibus.", "backing up Handcrafted Fresh Shoes challenge", 1 });

            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "CreatedAt", "Name", "ProjectId" },
                values: new object[] { 10, new DateTime(2019, 8, 25, 18, 48, 6, 62, DateTimeKind.Local).AddTicks(3310), "Denesik - Greenfelder", 1 });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { 1, new DateTime(2007, 1, 1, 8, 10, 18, 898, DateTimeKind.Local).AddTicks(8690), "Brandy.Witting@gmail.com", "Brandy", "Witting", new DateTime(2019, 1, 10, 5, 51, 56, 714, DateTimeKind.Local).AddTicks(8896), 10 });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "TaskState" },
                values: new object[,]
                {
                    { 1, new DateTime(2020, 5, 16, 1, 50, 46, 86, DateTimeKind.Local).AddTicks(832), "Quo sint aut et ea voluptatem omnis ut.", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "real-time", 1, 1, 1 },
                    { 2, new DateTime(2019, 2, 15, 18, 6, 52, 60, DateTimeKind.Local).AddTicks(666), "Eum a eum.", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "product Direct utilize", 1, 1, 1 },
                    { 3, new DateTime(2017, 8, 16, 9, 13, 44, 577, DateTimeKind.Local).AddTicks(3845), "Sint voluptatem quas.", new DateTime(2020, 9, 9, 9, 34, 47, 460, DateTimeKind.Local).AddTicks(2160), "bypass", 1, 1, 1 },
                    { 4, new DateTime(2018, 10, 19, 3, 58, 34, 804, DateTimeKind.Local).AddTicks(5103), "Delectus quibusdam id quia iure neque maiores molestias sed aut.", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "withdrawal contextually-based", 1, 1, 1 },
                    { 5, new DateTime(2018, 6, 15, 9, 3, 48, 73, DateTimeKind.Local).AddTicks(2466), "Earum blanditiis repellendus qui magni aliquam quisquam consequatur odio ducimus.", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "mobile Organized", 1, 1, 1 },
                    { 6, new DateTime(2020, 5, 21, 17, 56, 53, 811, DateTimeKind.Local).AddTicks(7818), "Reiciendis iusto rerum non et aut eaque.", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "world-class Circles", 1, 1, 1 },
                    { 7, new DateTime(2018, 7, 10, 19, 21, 12, 88, DateTimeKind.Local).AddTicks(6153), "Et rerum ad.", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Automotive & Tools transitional bifurcated", 1, 1, 1 },
                    { 8, new DateTime(2019, 5, 7, 23, 29, 10, 58, DateTimeKind.Local).AddTicks(2950), "Voluptas nostrum sint.", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "payment methodologies", 1, 1, 1 },
                    { 9, new DateTime(2020, 6, 19, 14, 42, 55, 73, DateTimeKind.Local).AddTicks(8847), "Rerum iure soluta consequatur velit aut.", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Borders Mountain", 1, 1, 1 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1);
        }
    }
}
