﻿using Common.DTO;
using HomeWork3ProjectStructure.Interfaces;
using System.Threading.Tasks;

namespace HomeWork3ProjectStructure.Queries
{
    public class GetProjectByIdQuerie : IQuerie<Task<ProjectDTO>>
    {
        public int Id { get; set; }
    }
}
