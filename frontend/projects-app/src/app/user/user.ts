export class User {
    Id : number = 0;
    TeamId : number;
    Name : string;
    Surname : string;
    Email : string;
    RegisteredAt : Date;
    BirthDay : Date;

    constructor(TeamId: number, Name: string, Surname: string, Email : string, RegisteredAt : Date, BirthDay : Date, Id?: number){
        this.Id = Id as number;
        this.TeamId = TeamId;
        this.Name = Name;
        this.Surname = Surname;
        this.Email = Email;
        this.RegisteredAt = RegisteredAt;
        this.BirthDay = BirthDay;
    }
}
