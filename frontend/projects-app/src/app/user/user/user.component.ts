import { Component, OnDestroy, Input } from '@angular/core';
import { User } from '../user';
import { UserListComponent } from '../user-list/user-list.component';
import { UserService } from '../user.service';


@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent {
  userService : UserService;
  userlist : UserListComponent;
  @Input() public user!: User;
  constructor(userService : UserService, userlist: UserListComponent) {
    this.userService = userService;
    this.userlist = userlist;
   }
  showUpdateUserForm : boolean = false;
  serverError : string = "";

  public updateUser(){
    console.log(this.user);
    this.userService.updateUser(this.user).subscribe((data)=> {
      this.user = data;
      this.showingUpdateUserForm();
    }, (error : Error) => {this.serverError = error.message;});
  }

  public showingUpdateUserForm(){
    this.showUpdateUserForm = !this.showUpdateUserForm;
  }

  public deleteUser(){
    console.log(this.user);
    this.userService.deleteUser(this.user.Id).subscribe((data)=> {
      this.userlist.users.splice(this.userlist.users.findIndex(x => x.Id === this.user.Id), 1)
    }, (error : Error) => {this.serverError = error.message;});
  }
}
