﻿using Common.DTO;
using HomeWork3ProjectStructure.Interfaces;
using System.Threading.Tasks;

namespace HomeWork3ProjectStructure.Commands
{
    public class UpdateUserCommand : ICommand<Task<UserDTO>>
    {
        public UserDTO user { get; set; }
    }
}
