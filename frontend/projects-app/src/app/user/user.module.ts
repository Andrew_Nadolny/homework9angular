import { NgModule, Pipe } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserComponent } from './user/user.component';
import { UserListComponent } from './user-list/user-list.component';
import { HttpClientModule } from '@angular/common/http'
import { FormsModule } from '@angular/forms';
import { UserGuard } from './guard/user.guard';
import { DatepipeModule } from '../datepipe/datepipe.module';

@NgModule({
    imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    DatepipeModule
  ],  
  declarations: [
    UserComponent,
    UserListComponent
  ],
  providers: [UserGuard],
  exports: [UserListComponent]
})


export class UserModule { 

}
