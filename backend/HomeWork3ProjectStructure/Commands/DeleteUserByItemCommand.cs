﻿using Common.DTO;
using HomeWork3ProjectStructure.Interfaces;
using System.Threading.Tasks;

namespace HomeWork3ProjectStructure.Commands
{
    public class DeleteUserByItemCommand : ICommand<Task<bool>>
    {
        public UserDTO user { get; set; }
    }
}
