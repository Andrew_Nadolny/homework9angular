import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Task } from './task';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  constructor(private http: HttpClient) { }

  getTasks(): Observable<Task[]> {
    return this.http.get<Task[]>('https://localhost:44309/api/task/', { headers: new HttpHeaders()}).pipe(map((data: any) => {
      return data.map(function (task: Task): Task {
        return new Task(task.Id, task.ProjectId, task.PerformerId, task.Name, task.Description, task.State, task.CreatedAt, task.FinishedAt);
      });
    }));
  }

  addNewTask(task : Task) { 
    return this.http.post<Task>(`https://localhost:44309/api/task/`, task);
  }

  updateTask(task : Task) { 
    return this.http.put<Task>(`https://localhost:44309/api/task/`, task);
  }

  deleteTask(taskId : number) { 
    return this.http.delete<Task>(`https://localhost:44309/api/task/${taskId}`);
  }
}
