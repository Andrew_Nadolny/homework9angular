import { Component, OnInit, Input } from '@angular/core';
import { Team } from '../team';
import { TeamListComponent } from '../team-list/team-list.component';
import { TeamService } from '../team.service';

@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.css']
})
export class TeamComponent {
  teamService : TeamService;
  teamlist : TeamListComponent;
  @Input() public team!: Team;
  constructor(teamService : TeamService, teamlist: TeamListComponent) {
    this.teamService = teamService;
    this.teamlist = teamlist;
   }
  showUpdateTeamForm : boolean = false;
  serverError : string = "";

  public updateTeam(){
    this.teamService.updateTeam(this.team).subscribe((data)=> {
      this.team = data;
      this.showingUpdateTeamForm();
    }, (error : Error) => {this.serverError = error.message;});
  }

  public showingUpdateTeamForm(){
    this.showUpdateTeamForm = !this.showUpdateTeamForm;
  }

  public deleteTeam(){
    console.log(this.team);
    this.teamService.deleteTeam(this.team.Id).subscribe((data)=> {
      this.teamlist.teams.splice(this.teamlist.teams.findIndex(x => x.Id === this.team.Id), 1)
    }, (error : Error) => {this.serverError = error.message;});
  }
}
