﻿using AutoMapper;
using Common.DTO;
using HomeWork3ProjectStructure.Commands;
using HomeWork3ProjectStructure.DAL;
using HomeWork3ProjectStructure.Handlers;
using HomeWork3ProjectStructure.Processors;
using HomeWork3ProjectStructure.Queries;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace HomeWork3ProjectStructure.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectController : ControllerBase
    {
        private readonly QuerieProcessor _querieProcessor;
        private readonly CommandProcessor _commandProcessor;

        public ProjectController(ProjectsDbContext context, IMapper mapper)
        {
            _querieProcessor = new QuerieProcessor(new QuerieHandler(context, mapper));
            _commandProcessor = new CommandProcessor(new CommandHandler(context, mapper));
        }
        // GET: api/<ProjectController>
        [HttpGet]
        public async System.Threading.Tasks.Task<string> GetAsync()
        {
            return JsonConvert.SerializeObject(await _querieProcessor.ProcessedAsync(new GetProjectsQuerie()));
        }

        // GET api/<ProjectController>/5
        [HttpGet("{id}")]
        public async System.Threading.Tasks.Task<string> GetAsync(int id)
        {
            return JsonConvert.SerializeObject(await _querieProcessor.ProcessedAsync(new GetProjectByIdQuerie() { Id = id }));
        }

        // POST api/<ProjectController>
        [HttpPost]
        public async System.Threading.Tasks.Task<IActionResult> PostAsync([FromBody] ProjectDTO project)
        {
            try
            {
                return Ok(JsonConvert.SerializeObject(await _commandProcessor.ProcessedAsync(new CreateProjectCommand() { project = project })));
            }
            catch (ArgumentException)
            {
                return BadRequest();
            }
        }

        // PUT api/<ProjectController>/5
        [HttpPut]
        public async System.Threading.Tasks.Task<string> Put([FromBody] ProjectDTO project)
        {
            return JsonConvert.SerializeObject(await _commandProcessor.ProcessedAsync(new UpdateProjectCommand() { project = project }));

        }

        // DELETE api/<ProjectController>/5
        [HttpDelete("{id}")]
        public async System.Threading.Tasks.Task<bool> DeleteAsync(int id)
        {
            return await _commandProcessor.ProcessedAsync(new DeleteProjectByIdCommand() { Id = id });
        }

        [HttpDelete]
        public async System.Threading.Tasks.Task<bool> Delete([FromBody] ProjectDTO project)
        {
            return await _commandProcessor.ProcessedAsync(new DeleteProjectByItemCommand() { project = project });

        }
    }
}
