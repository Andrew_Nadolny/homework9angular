﻿using Common.Models;
using HomeWork3ProjectStructure.DAL;
using HomeWork3ProjectStructure.Repositores;
using System.Threading.Tasks;


namespace HomeWork3ProjectStructure.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private ProjectsDbContext _context;
        public UnitOfWork(ProjectsDbContext context)
        {
            _context = context;
        }
        public IRepository<TEntity> Set<TEntity>() where TEntity : Entity
        {
            return new Repository<TEntity>(_context);
        }

        public async Task<int> SaveChangesAsync()
        {
            return await _context.SaveChangesAsync();
        }
    }
}
