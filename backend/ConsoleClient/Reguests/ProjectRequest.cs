﻿using AutoMapper;
using Common.DTO;
using Common.MappingProfiles;
using Common.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace ConsoleClient.Reguests
{
    public class ProjectRequest
    {
        public string ApiUrl = "https://localhost:44309";
        private Mapper _mapper;
        HttpClient httpClient;
        public ProjectRequest()
        {
            httpClient = new HttpClient();
            httpClient.BaseAddress = new Uri(ApiUrl);
            _mapper = new Mapper(new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<ProjectProfile>();
                cfg.AddProfile<TeamProfile>();
                cfg.AddProfile<TaskProfile>();
                cfg.AddProfile<UserProfile>();

            }));
        }

        public async Task<List<Project>> GetProjectsAsync()
        {
            using (HttpClient httpClient = new HttpClient())
            {
                List<Project> projects = new List<Project>();
                httpClient.BaseAddress = new Uri(ApiUrl);
                HttpResponseMessage response = await httpClient.GetAsync("/api/Project");
                if (response.IsSuccessStatusCode)
                {
                    projects = _mapper.Map<List<Project>>(JsonConvert.DeserializeObject<List<ProjectDTO>>(await response.Content.ReadAsStringAsync()));
                }
                return projects;
            }
        }
        public async Task<Project> GetProjectAsync(int projectId)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                Project project = new Project();
                httpClient.BaseAddress = new Uri(ApiUrl);
                HttpResponseMessage response = await httpClient.GetAsync(string.Format("/api/Project/{0}", projectId));
                if (response.IsSuccessStatusCode)
                {
                    project = _mapper.Map<Project>(JsonConvert.DeserializeObject<ProjectDTO>(await response.Content.ReadAsStringAsync()));
                }
                return project;
            }
        }
    }
}
