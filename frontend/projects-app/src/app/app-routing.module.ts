import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProjectListComponent } from './project/project-list/project-list.component';
import { UserListComponent } from './user/user-list/user-list.component';
import { TeamListComponent } from './team/team-list/team-list.component';
import { TaskListComponent } from './task/task-list/task-list.component';
import { UserGuard } from './user/guard/user.guard';


const appRoutes: Routes =[
  { path: 'projects', component: ProjectListComponent},
  { path: 'users', canDeactivate: [UserGuard], component: UserListComponent},
  { path: 'teams', component: TeamListComponent,},
  { path: 'tasks', component: TaskListComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  providers: [UserGuard],
  exports: [RouterModule]
})
export class AppRoutingModule { }
