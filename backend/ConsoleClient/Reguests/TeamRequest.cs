﻿using AutoMapper;
using Common.DTO;
using Common.MappingProfiles;
using Common.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace ConsoleClient.Reguests
{
    class TeamRequest
    {
        public string ApiUrl = "https://localhost:44309";
        private Mapper _mapper;
        HttpClient httpClient;
        public TeamRequest()
        {
            httpClient = new HttpClient();
            httpClient.BaseAddress = new Uri(ApiUrl);
            _mapper = new Mapper(new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<ProjectProfile>();
                cfg.AddProfile<TeamProfile>();
                cfg.AddProfile<TaskProfile>();
                cfg.AddProfile<UserProfile>();

            }));
        }
        public async Task<List<Team>> GetTeamsAsync()
        {
            using (HttpClient httpClient = new HttpClient())
            {
                List<Team> teams = new List<Team>();
                httpClient.BaseAddress = new Uri(ApiUrl);
                HttpResponseMessage response = await httpClient.GetAsync("/api/Team");
                if (response.IsSuccessStatusCode)
                {
                    teams = _mapper.Map<List<Team>>(JsonConvert.DeserializeObject<List<Team>>(await response.Content.ReadAsStringAsync()));
                }
                return teams;
            }
        }

        public async Task<Team> GetTeamAsync(int teamId)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                Team team = new Team();
                httpClient.BaseAddress = new Uri(ApiUrl);
                HttpResponseMessage response = await httpClient.GetAsync(string.Format("/api/Team/{0}", teamId));
                if (response.IsSuccessStatusCode)
                {
                    team = _mapper.Map<Team>(JsonConvert.DeserializeObject<TeamDTO>(await response.Content.ReadAsStringAsync()));
                }
                return team;
            }
        }
    }
}
