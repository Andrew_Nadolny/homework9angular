﻿using HomeWork3ProjectStructure.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HomeWork3ProjectStructure.Queries
{
    public class GetCountTasksInProjectByAuthorIdQuerie : IQuerie<Task<Dictionary<int, int>>>
    {
        public int Id { get; set; }
    }
}
