﻿using HomeWork3ProjectStructure.Interfaces;
using System.Threading.Tasks;

namespace HomeWork3ProjectStructure.Commands
{
    public class DeleteProjectByIdCommand : ICommand<Task<bool>>
    {
        public int Id { get; set; }
    }
}
